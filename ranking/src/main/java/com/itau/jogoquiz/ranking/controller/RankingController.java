package com.itau.jogoquiz.ranking.controller;

import java.util.Calendar;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.jogoquiz.ranking.model.Ranking;
import com.itau.jogoquiz.ranking.repository.RankingRepository;

@RestController
@RequestMapping("/ranking")
public class RankingController {

	
	@Autowired
	RankingRepository rankingRepository ;
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<Ranking> getRanking(){
		return rankingRepository.findAll();
	}
	@RequestMapping(path="/{id}",method=RequestMethod.GET)
	public Optional<Ranking> getRankingById(Long id){
		return rankingRepository.findById(id);
	}
	
	@RequestMapping(path="/info",method=RequestMethod.GET)
	public ResponseEntity<?> hello(){
		return ResponseEntity.ok().body("HELOOOO!");
	}
	@RequestMapping(path="/jogo/{idJogo}",method=RequestMethod.GET)
	public ResponseEntity<Iterable<Ranking>> getRankingByIdJogo(@PathVariable Integer idJogo){
		
		try {
			return ResponseEntity.ok().body(rankingRepository.findByIdJogo(idJogo));
			
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
		
	}
	
	@RequestMapping(path="/top/{idJogo}", method=RequestMethod.GET)
	public ResponseEntity<Iterable<Ranking>> listTopJogadorPorJogo(@PathVariable Integer idJogo){
		return ResponseEntity.ok(rankingRepository.findTop10ByIdJogoOrderByPontosDesc(idJogo));
	}
		
	@RequestMapping(path="/{idJogador}/{idJogo}", method=RequestMethod.GET)
	public Ranking getRankingJogador(@PathVariable Integer idJogador,@PathVariable Integer idJogo ){
		return rankingRepository.findByIdJogadorAndIdJogo(idJogador, idJogo);
	}

	@RequestMapping(path="/", method=RequestMethod.PUT)
	public ResponseEntity<Ranking> incluirRanking(@RequestBody Ranking rank){
		
		try {
			Ranking rankAtualizar = rankingRepository.findByIdJogadorAndIdJogo(rank.getIdJogador(), rank.getIdJogo());
			
			
			rankAtualizar.setPontos(rank.getPontos() + rankAtualizar.getPontos());
			rankAtualizar.setDataUpdate(Calendar.getInstance());
	
			Ranking rankingNovo = rankingRepository.save(rankAtualizar);
			
			return ResponseEntity.status(201).body(rankingNovo);
		}catch(Exception e ) {
			
			try {
				
				Ranking rankingNovo = rankingRepository.save(rank);
				return ResponseEntity.status(201).body(rankingNovo);
				
			} catch (Exception e2) {
				return ResponseEntity.badRequest().build();
			}
		}
		
	}
	
	
}
