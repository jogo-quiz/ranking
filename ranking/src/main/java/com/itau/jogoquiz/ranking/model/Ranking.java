package com.itau.jogoquiz.ranking.model;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Ranking {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private Integer idJogador;
	private Integer idJogo;
	private Long pontos;
	private Calendar dataCriacao;
	private Calendar dataUpdate;
	
	public Integer getIdJogador() {
		return idJogador;
	}
	public void setIdJogador(Integer idJogador) {
		this.idJogador = idJogador;
	}
	public Integer getIdJogo() {
		return idJogo;
	}
	public void setIdJogo(Integer idJogo) {
		this.idJogo = idJogo;
	}
	public Long getPontos() {
		return pontos;
	}
	public void setPontos(Long pontos) {
		this.pontos = pontos;
	}
	public Calendar getDataCriacao() {
		return dataCriacao;
	}
	public void setDataCriacao(Calendar dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
	public Calendar getDataUpdate() {
		return dataUpdate;
	}
	public void setDataUpdate(Calendar dataUpdate) {
		this.dataUpdate = dataUpdate;
	}
	
	
	

}
