package com.itau.jogoquiz.ranking.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.itau.jogoquiz.ranking.model.Ranking;

public interface RankingRepository extends CrudRepository<Ranking, Long>{
	public Ranking findByIdJogadorAndIdJogo (Integer idJogador, Integer idJogo);
	public List<Ranking> findTop10ByIdJogoOrderByPontosDesc(Integer idJogo);
	public List<Ranking> findByIdJogo(Integer idJogo);
	

}
